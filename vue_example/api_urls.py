from django.urls import path, include, re_path
from . import api_views
from rest_framework import routers

router = routers.DefaultRouter()

urlpatterns = [
    re_path(r'', include(router.urls)),
    path('get-all-post', api_views.AllPost.as_view()),
    path('post/<int:pk>' , api_views.PostDetailView.as_view())

]
