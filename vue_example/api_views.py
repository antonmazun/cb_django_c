from django.core import serializers

from django.shortcuts import get_object_or_404
from rest_framework import viewsets, generics
from .serializers import PostSerializer
from rest_framework.response import Response
from .models import Post
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework import status


class AllPost(APIView):

    # if request.method == 'GET':
        # self.get()
    def get(self, request, format=None):
        all_post = Post.objects.all()
        serializer = PostSerializer(all_post, many=True)
        return Response(serializer.data)

    def post(self, request):
        x = PostSerializer(data=request.data)
        if x.is_valid():
            x.save()
        return Response({
            'ok': True
        })


class PostDetailView(APIView):

    def get(self, request, pk):
        post = get_object_or_404(Post, pk=pk)
        serializer = PostSerializer(post)
        return Response(serializer.data)

    def delete(self, request, pk):
        post = get_object_or_404(Post, pk=pk)
        post.delete()
        content = {'status': True}
        return Response(content)

    def put(self, request, pk):
        post = get_object_or_404(Post, pk=pk)
        serializer = PostSerializer(post, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
