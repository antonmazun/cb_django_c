from django.urls import path, include, re_path
from . import views

app_name = 'vue'
urlpatterns = [
    path('', views.main_vue, name='main'),
    path('api/', include('vue_example.api_urls')),

]
