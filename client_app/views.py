from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.conf import settings
from django.contrib.auth.decorators import login_required
from .forms import AuthorForm, ArticleForm, CommentForm
from .models import Author, Article
from django.contrib.auth.models import User
from lesson_3.models import Book


@login_required(redirect_field_name=settings.LOGIN_URL)
def cabinet(request):
    ctx = {}
    ctx['cabinet_tab'] = 'main'
    if request.method == 'GET':
        try:
            author = Author.objects.get(user=request.user.id)
            ctx['form'] = AuthorForm(instance=author)
        except Exception as e:
            ctx['form'] = AuthorForm
    elif request.method == 'POST':
        form = AuthorForm(request.POST)
        if form.is_valid():
            author = Author.objects.get(user=request.user.id)
            author.date_birth = form.cleaned_data['date_birth']
            author.bio = form.cleaned_data['bio']
            author.type_view = form.cleaned_data['type_view']
            author.pseudoname = form.cleaned_data['pseudoname']
            author.save()
            ctx['save'] = True
            ctx['form'] = AuthorForm(instance=author)
        else:
            ctx['form'] = AuthorForm(request.POST)
    return render(request, 'client_app/cabinet.html', ctx)


@login_required(redirect_field_name=settings.LOGIN_URL)
def write_post(request):
    ctx = {}
    ctx['article_form'] = ArticleForm
    ctx['cabinet_tab'] = 'write_post'
    if request.method == 'POST':
        form = ArticleForm(request.POST, request.FILES)
        if form.is_valid():
            author = Author.objects.get(user=request.user.id)
            form.save(author=author)
        else:
            ctx['article_form'] = form
    return render(request, 'client_app/write_post.html', ctx)


@login_required(redirect_field_name=settings.LOGIN_URL)
def my_articles(request):
    ctx = {}
    all_articles = Article.objects.filter(author=Author.objects.get(user=request.user.id))
    ctx['cabinet_tab'] = 'my_articles'
    ctx['all_articles'] = all_articles
    print(all_articles, len(all_articles))
    return render(request, 'client_app/my_articles.html', ctx)


def article_item(request, pk):
    ctx = {}
    article = Article.objects.get(pk=pk)
    ctx['article'] = article
    ctx['comment_form'] = CommentForm()
    return render(request, 'client_app/show_article.html', ctx)


from django.contrib import messages


def add_comment(request, article_id):
    ctx = {}
    article = Article.objects.get(pk=article_id)
    ctx['article'] = article
    print(request.GET)
    if not request.user.is_authenticated:
        ctx['messages'] = "Login please!"
        # ctx['article_id'] = article_id
        return render(request, 'client_app/show_article.html', ctx)
    if request.method == 'POST':
        print('asdasdassdas dd ad ada da dasd as')
        form = CommentForm(request.POST)
        if form.is_valid():
            article = Article.objects.get(pk=article_id)
            print('add_comment')
            print(request.user)
            print('add_comment')
            new_comment = form.save(from_user=request.user, commit=False)
            article.comments.add(new_comment)
            article.save()
            return redirect('client_app:article-item', pk=article_id)
    return redirect(request.get_absolute_url)


def filter_by_status(request, status):
    ctx = {}
    ctx['all_articles'] = None
    return render(request, 'client_app/my_articles.html', ctx)


def change_color(request):
    if request.method == 'POST':
        request.session['color'] = request.POST.get('color')  # #DF1565
    return redirect('client_app:cabinet')


def all_articles(request):
    ctx = {

    }
    all_articles = Article.objects.filter(status='publish')
    ctx['list_of_article'] = all_articles
    return render(request, 'client_app/all_articles.html', ctx)


def wish_list(request, pk):
    ctx = {}
    article_ids = request.session.get('read_article', [])
    print(article_ids)
    if pk:
        print(pk, '----------------')
        if pk in article_ids:
            article_ids.remove(pk)
            request.session['read_article'] = article_ids
    ctx['all_articles'] = [Book.objects.get(pk=elem) for elem in article_ids]
    print(article_ids)
    return render(request, 'client_app/wish_list.html', ctx)


def set_to_session(request):
    saved_list = request.session.get('read_article', [])
    saved_list.append(int(request.GET.get('article_id')))
    request.session['read_article'] = saved_list
    # request.session.clear()
    return redirect('lesson_3:home')


from django.template.loader import render_to_string


def delete_article(request, pk):
    if request.is_ajax():
        Article.objects.get(pk=int(pk)).delete()
        all_articles = Article.objects.filter(author=Author.objects.get(user=request.user.id))
        response_html = render_to_string('client_app/ajax/reload_block_articles.html', {
            'all_articles': all_articles
        })
        return JsonResponse({
            'deleted': True,
            'response_html': response_html,
            'count_articles': len(all_articles)
        })


def show_article(request, pk):
    content = Article.objects.get(id=int(pk)).content
    return JsonResponse({
        'content': content
    })


def show_article_client(request, pk):
    article = Article.objects.get(pk=pk)
    return render(request, 'client_app/show_article_client.html', {
        'article': article
    })
