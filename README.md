Setup project.

You should clone this project via link from gitlab.

1) git clone https://gitlab.com/antonmazun/cb_django_c.git

2) In the project folder you should create virtual environment  "python -m venv env" (if your OS is MACOs , this command is  "python3 -m venv env")

3) Next step : activate env. On windows  - "cd env/Scripts " and run script "activate". On MACOS or Unix system  - source env/bin/activate.

4) In the folder where it is located file "req.txt" you should run command pip install -r req.txt 

5) python manage.py migrate apply all migrations for django :)

6) python manage.py makemigrations  - create file of migrations

7) python manage.py migrate  - apply your changes in models.py to DB

8) python manage.py createsuperuser  - create admin user.

9) python manage.py runserver 9000 (optional you can changes port  , default port - 8000)

That`s all.
