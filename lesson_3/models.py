from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Author(models.Model):
    CHOICE_PREVIEW = (
        ('pseudoname', 'Псевдоним'),
        ('full_name', 'Полное имя')
    )
    name = models.CharField(max_length=50, verbose_name='Name author')
    surname = models.CharField(max_length=100,
                               verbose_name='Surname author',
                               blank=True, null=True)
    type_preview = models.CharField(max_length=100, choices=CHOICE_PREVIEW)

    def __str__(self):
        return '{} {}'.format(self.name, self.surname)

    def get_full_name(self):
        return '{} {}'.format(self.name, self.surname)

    def books(self):
        return Book.objects.filter(author=self)


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True, related_name='comments_books')
    date = models.DateTimeField(auto_now_add=True)
    msg = models.TextField(max_length=1000, verbose_name='Ваш коментарий')

    def __str__(self):
        return '{user} {msg}'.format(user=self.user, msg=self.msg)


class Book(models.Model):
    author = models.ForeignKey(Author, on_delete=models.CASCADE,
                               verbose_name='Автор книги', default='', blank=True, null=True)
    title = models.CharField(max_length=255)
    text = models.TextField(max_length=500)
    preview = models.ImageField(upload_to='avatar-book/', default='', blank=True, null=True)
    price = models.FloatField()
    comments = models.ManyToManyField(Comment, blank=True, null=True)
    
    def __str__(self):
        return "{} {}".format(self.title, self.text[:50] + '...' if len(self.text) > 50 else self.text)

    class Meta:
        verbose_name = 'Книга'
        verbose_name_plural = "Книгы"
        ordering = ['-id']
