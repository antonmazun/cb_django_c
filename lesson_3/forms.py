from django import forms
from .models import Comment


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['msg']

    def save(self, *args, **kwargs):
        print(self.instance)
        self.instance.from_user = kwargs['from_user']
        self.instance.save()
        return self.instance
