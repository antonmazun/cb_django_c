from django.shortcuts import render, redirect
from .models import Book, Author
from .forms import CommentForm


# Create your views here.


def index(request):
    ctx = {}
    all_books = Book.objects.all()  # SELECT * FROM book;
    ctx['all_books'] = all_books
    return render(request, 'pages/home.html', ctx)


def all_authors(request):
    return render(request, 'pages/authors.html', {
        'authors': Author.objects.all()
    })


def book_info(request, pk):
    current_book = Book.objects.get(id=pk)
    ctx = {
        'object': current_book,
        'form': CommentForm
    }
    if request.method == 'GET':
        return render(request, 'pages/book_info.html', ctx)
    elif request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            current_user = request.user
            new_comment = form.save(from_user=current_user)
            new_comment.user = current_user
            new_comment.save()
            current_book.comments.add(new_comment)
            current_book.save()
            print(new_comment)
        return redirect('lesson_3:book-info', pk=pk)


def all_book_for_authors(request, id_author):
    current_author = Author.objects.get(id=id_author)
    return render(request, 'pages/home.html', {
        'all_books': current_author.books(),
        'author_page': True
    })


def calc(request):
    ctx = {}
    calc_obj = {
        '+': lambda a, b: a + b,
        '-': lambda a, b: a - b,
        '*': lambda a, b: a * b,
        '/': lambda a, b: a / b,
    }
    ctx['operations'] = calc_obj.keys()
    if request.method == 'POST':
        check = request.POST.get('check')
        print(bool(check))
        try:
            first_number = float(request.POST.get('first_number'))
            operation = request.POST.get('operation')
            second_number = float(request.POST.get('second_number'))
            result = calc_obj[operation](first_number, second_number)
            ctx.update({
                'result': result
            })
        except Exception as e:
            ctx.update({
                'result': e
            })
    return render(request, 'pages/calc.html', ctx)
