from django.urls import path, include, re_path
from . import views

app_name = 'lesson_3'

urlpatterns = [
    path('', views.index, name='home'),
    path('all-authors', views.all_authors, name='authors'),
    path('book-info/<int:pk>', views.book_info, name='book-info'),
    path('all_book_for_authors/<int:id_author>',
         views.all_book_for_authors,
         name='all_book_for_authors'),
    path('calc', views.calc, name='calc'),
]
