function delete_article() {
    $('.js-delete').click(function (event) {
        event.preventDefault();
        var ajax_url = $(this).attr('href');
        console.log(ajax_url);
        $.ajax({
            url: ajax_url,
            method: 'GET'
        }).done(function (response) {
            console.log(response);
            if (response.deleted) {
                alert('удаление прошло успешно!');
            }
            $('.js-article-reload').empty();
            $('.js-article-reload').append(response.response_html);
            $('.js-count').text(response.count_articles);
            console.log('re init delete article');
            re_init_atter_delete();
        });
    })
}


function show_content_popup() {
    $('.js-show-content').click(function (e) {
        e.preventDefault();
        $('.popup-wrapper').show();

        $.ajax({
            url: $(this).attr('href'),
            method: 'GET'
        }).done(function (resp) {
            console.log(resp.content);
            $('.popup-wrapper').find('.content').html(resp.content);
            console.log(resp.content);
        }).fail(function (error) {
            console.log('error');
            console.log(error);
        })
    })
}

function popup_close() {
    $('.js-close').click(function () {
        $('.popup-wrapper').hide();
    })
}


delete_article();

show_content_popup();

popup_close();


function re_init_atter_delete() {
    delete_article();
    show_content_popup();
    popup_close();
}
